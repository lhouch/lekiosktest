//
//  WSMethods.swift
//  TestLeKiosk
//
//  Created by lhouch mohamed on 5/27/19.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import UIKit
import Alamofire

func GetApiRequest(_ method: HTTPMethod, url: String,nameHeader : String, header : String, completion:@escaping (_ finished: Bool, _ response: AnyObject?) ->Void) {
    
    //let headers = ["X-ENTITY": header]
    let headers = [nameHeader == "" ? "X-ENTITY" : nameHeader : header]
    
    let url_ = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    
    Alamofire.request(url_, method: method, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
        
        switch(response.result) {
            
        case .success(_):
            
            if let data = response.result.value{
                
                completion(true, data as AnyObject?)
            }
            
            break
            
        case .failure(_):
            
            completion(false, response.result.error as AnyObject?)
            
            break
        }
    }
}

