//
//  WSService.swift
//  TestLeKiosk
//
//  Created by lhouch mohamed on 5/27/19.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import UIKit
import Alamofire

class WSService: NSObject {
    
    static let sharedInstance = WSService()
    var dictionary =  Dictionary<String, Any>()
    var resultContent : Array<Any>?
    
    var listUsers = [User]()
    var listTasks = [Task]()
    
    typealias CompletionHandlerListUser = (_ statusCode : String ,_ msg : String, _ listUser : [User]) -> Void
    typealias CompletionHandlerListTask = (_ statusCode : String ,_ msg : String, _ listTask : [Task]) -> Void
    
    
    func getListUsers(completionHandler: @escaping CompletionHandlerListUser) {
        
        Alamofire.request(USERS_URL_WS).responseJSON { response in
            switch(response.result) {
            case .success(_):
                if let json = response.data{
                    do{
                        //using the array to put values
                        self.listUsers = try JSONDecoder().decode([User].self, from: json)
                        DispatchQueue.global().async {
                            DataCacheManager.sharedInstance.saveUserss(data : self.listUsers)
                        }
                        completionHandler("success" ,"", self.listUsers)
                    }catch let err{
                        completionHandler("error" ,err.localizedDescription , DataCacheManager.sharedInstance.geListtUser())
                    }
                }
                break
            case .failure(_):
                completionHandler("error" ,"Aucun accès à Internet" , DataCacheManager.sharedInstance.geListtUser())
                break
            }
        }
    }
    
    
    func getListTasks(userID : Int, completionHandler: @escaping CompletionHandlerListTask) {
        
        Alamofire.request(TASK_URL_WS + String(userID)).responseJSON { response in
            
            switch(response.result) {
                
            case .success(_):
                if let json = response.data{
                    do{
                        self.listTasks = try JSONDecoder().decode([Task].self, from: json)
                        completionHandler("success" ,"", self.listTasks)
                        DispatchQueue.global().async {
                            DataCacheManager.sharedInstance.addTaskToUser(tasks: self.listTasks, userID: userID)
                        }
                    }catch let err{
                        
                        completionHandler("error" ,err.localizedDescription , [])
                    }
                }
                
                break
                
            case .failure(_):
                if (DataCacheManager.sharedInstance.findUserById(userID: userID).tasks != nil) {
                    self.listTasks = DataCacheManager.sharedInstance.findUserById(userID: userID).tasks!
                }else{
                    self.listTasks = []
                    
                }
                completionHandler("error" ,"Aucun accès à Internet" , self.listTasks)
                
                break
            }
        }
    }
    
}
