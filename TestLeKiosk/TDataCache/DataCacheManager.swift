//
//  DataManager.swift
//  TestLeKiosk
//
//  Created by lhouch mohamed on 29/05/2019.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import Foundation

class DataCacheManager : NSObject
{
    
    static let sharedInstance = DataCacheManager()
    // Checking the UserDefaults is saved or not
    func didSave(preferences: UserDefaults){
        let didSave = preferences.synchronize()
        if !didSave{
            // Couldn't Save
            print("Preferences could not be saved!")
        }
    }
    func saveUserss(data: [User]) {
        let preferences = UserDefaults.standard
        preferences.set(try? PropertyListEncoder().encode(data), forKey: USERDATA)
        didSave(preferences: preferences)
    }
    
    func geListtUser()->[User]
    {
        var mData:[User]
        if let data = UserDefaults.standard.value(forKey: USERDATA) as? Data {
            let datas = try? PropertyListDecoder().decode([User].self, from: data)
            mData = datas! as [User]
        }else
        {
            mData = [User]()
        }
        return mData
    }
    
    
    func addTaskToUser(tasks : [Task], userID : Int){
        var mData:[User]
        if let data = UserDefaults.standard.value(forKey: USERDATA) as? Data {
            let datas = try? PropertyListDecoder().decode([User].self, from: data)
            mData = datas! as [User]
            if let row = mData.firstIndex(where: {$0.id == userID}) {
                mData[row].tasks = tasks
            }
            saveUserss(data: mData)
            
        }
    }
    
    func findUserById(userID : Int) -> User {
        var mData:[User]
        var user : User = User()
        if let data = UserDefaults.standard.value(forKey: USERDATA) as? Data {
            let datas = try? PropertyListDecoder().decode([User].self, from: data)
            mData = datas! as [User]
            if let row = mData.firstIndex(where: {$0.id == userID}) {
                user = mData[row]
            }
        }
        return user
    }
    
    
}
