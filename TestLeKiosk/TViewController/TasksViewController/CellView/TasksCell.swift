//
//  TasksCell.swift
//  TestLeKiosk
//
//  Created by lhouch mohamed on 29/05/2019.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import UIKit

class TasksCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var etat: UILabel!
    
    func initCell(task : Task){
        self.title.text = task.title
        self.etat.text = task.completed! ? "completed" : ""
    }
    
}
