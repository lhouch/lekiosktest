//
//  GridLayout.swift
//  TestLeKiosk
//
//  Created by lhouch mohamed on 29/05/2019.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import UIKit

class GridLayout: UICollectionViewFlowLayout {
    
    var numberOfColumns: Int = 3
    var heightCell: Int = 130
    
    init(numberOfColumns: Int,heightCell: Int, spacing: Int ) {
        super.init()
        minimumLineSpacing = CGFloat(spacing) * 2
        minimumInteritemSpacing = CGFloat(spacing)
        
        self.heightCell = heightCell
        self.numberOfColumns = numberOfColumns
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var itemSize: CGSize {
        get {
            if let collectionView = collectionView {
                let itemWidth: CGFloat = (collectionView.frame.width/CGFloat(self.numberOfColumns)) - self.minimumInteritemSpacing
                let itemHeight: CGFloat = CGFloat(heightCell)
                return CGSize(width: itemWidth, height: itemHeight)
            }
            
            // Default fallback
            return CGSize(width: 100, height: heightCell)
        }
        set {
            super.itemSize = newValue
        }
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return proposedContentOffset
    }
    
}
