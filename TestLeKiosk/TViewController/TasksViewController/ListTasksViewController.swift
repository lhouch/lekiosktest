//
//  ListTasksViewController.swift
//  TestLeKiosk
//
//  Created by lhouch mohamed on 29/05/2019.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import UIKit

class ListTasksViewController: UIViewController {
    @IBOutlet weak var mCollectionTasks: UICollectionView!
    var mListTask : [Task] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        initCollectionView()
    }
    
    func initCollectionView() {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            mCollectionTasks.collectionViewLayout = GridLayout(numberOfColumns: 2, heightCell: 228,spacing: 5)
            break;
        case .pad:
            mCollectionTasks.collectionViewLayout = GridLayout(numberOfColumns: 4, heightCell: 228,spacing: 7)
            break;
        case .unspecified:
            mCollectionTasks.collectionViewLayout = GridLayout(numberOfColumns: 2, heightCell: 228,spacing: 5)
            break;
        case .tv:
            mCollectionTasks.collectionViewLayout = GridLayout(numberOfColumns: 4, heightCell: 228,spacing: 7)
            break;
        case .carPlay:
            mCollectionTasks.collectionViewLayout = GridLayout(numberOfColumns: 4, heightCell: 228,spacing: 7)
            break;
        @unknown default:
            mCollectionTasks.collectionViewLayout = GridLayout(numberOfColumns: 4, heightCell: 228,spacing: 5)
        }
        mCollectionTasks.reloadData()
    }
    
    
    
}

extension ListTasksViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tasksCell", for: indexPath) as! TasksCell
        cell.initCell(task: self.mListTask[indexPath.row])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mListTask.count
    }
    
    
    
}
