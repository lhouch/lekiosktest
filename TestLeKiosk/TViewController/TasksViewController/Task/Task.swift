//
//  Task.swift
//  TestLeKiosk
//
//  Created by lhouch mohamed on 29/05/2019.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import UIKit

struct Task: Codable {
    var id : Int?
    var title : String?
    var completed: Bool?
   
}
