//
//  User.swift
//  TestLeKiosk
//
//  Created by lhouch mohamed on 5/27/19.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//


import UIKit

struct User: Codable {
    var id : Int?
    var name : String?
    var username : String?
    var email : String?
    var tasks : [Task]?
}
