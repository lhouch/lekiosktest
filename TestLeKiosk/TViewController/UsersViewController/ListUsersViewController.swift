//
//  ViewController.swift
//  TestLeKiosk
//
//  Created by lhouch mohamed on 5/26/19.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import UIKit
class ListUsersViewController: UIViewController {
    
    @IBOutlet weak var mTableUser: UITableView!
    var mListUser : [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        getListUsers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func getListUsers() {
        self.showActivityIndicatoryInSuperview()
        WSService.sharedInstance.getListUsers { (status, msg , listUser) in
            self.hideActivityIndicatoryInSuperview()
            
            if status.elementsEqual("success")
            { 
                self.mListUser = listUser
                self.mTableUser.reloadData()
            }
            else
            {
                
                if !listUser.isEmpty{
                    self.mListUser = listUser
                    self.mTableUser.reloadData()
                }
                else{
                    self.showAlertViewInSuperview("Error", message: msg, completion: {
                        print(msg)
                    })
                }
                
            }
            
            
        }
    }
    
    
    func getListTasks(user : User) {
        self.showActivityIndicatoryInSuperview()
        WSService.sharedInstance.getListTasks(userID: user.id!) { (status, msg, listTask) in
            self.hideActivityIndicatoryInSuperview()
            
            if status.elementsEqual("success")
            {
                self.goToListTask(listTask: listTask)
            }
            else
            {
                if !listTask.isEmpty{
                    self.goToListTask(listTask: listTask)
                }
                else{
                    self.showAlertViewInSuperview("Error", message: msg, completion: {
                        print(msg)
                    })
                }
                
            }
        }
        
    }
    
    func goToListTask(listTask : [Task]) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "listTasksViewController") as! ListTasksViewController
        vc.mListTask = listTask
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ListUsersViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mListUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as! UserCell
        
        // Configure the cell...
        cell.initcell(user: self.mListUser[indexPath.row] )
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.getListTasks(user: self.mListUser[indexPath.row])
    }
    
    
}
