//
//  MGlobal.swift
//  TestLeKiosk
//
//  Created by lhouch mohamed on 5/26/19.
//  Copyright © 2019 lhouch mohamed. All rights reserved.
//

import UIKit

/*****************API Key***********************/
let BASE_URL = "https://jsonplaceholder.typicode.com"
let USERS_URL_WS = BASE_URL + "/users/"
let TASK_URL_WS = BASE_URL + "/todos?userId="

/*****************Key UIView***********************/
var ActivityIndicatorViewInSuperviewAssociatedObjectKey = "_UIViewActivityIndicatorViewInSuperviewAssociatedObjectKey";

let USERDATA = "USER_DATA"

